{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Oxygen intake per human breath\n",
    "First - calculate how much O<sub>2</sub> a human breathes. \n",
    "\n",
    "### Strategy 1\n",
    "\n",
    "Calculate the breath volume from the number of breaths per minute and the rate of air consumption. \n",
    "\n",
    "Quoting from Flamholz, Milo and Phillips MBoC 2014: \n",
    "A human at rest breathes in ≈6 liters of air per minute, containing ≈20% oxygen. The gas that we exhale contains ≈15% oxygen  Thus we consume ≈0.3 liter of oxygen/min (Burton, 2000). \n",
    "\n",
    "\n",
    "This [reference](https://my.clevelandclinic.org/health/articles/10881-vital-signs) from the Cleveland Clinic gives 12-20 breaths for an adult at rest. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Estimate #1 from total air influx per minute and number of breaths\n",
      "0.3 liters of air per breath\n",
      "0.1 liters O2 per breath\n",
      "0.003 mols O2 per breath\n"
     ]
    }
   ],
   "source": [
    "R = 8.2057e-2   # Gas constant in L atm / (mol K)\n",
    "T = 273.15 + 25 # 25 C in Kelvin\n",
    "RT = R*T\n",
    "\n",
    "oxygen_partial_pressure = 0.21                          # atmospheres\n",
    "mol_oxygen_per_liter_air = oxygen_partial_pressure / RT # mols\n",
    "\n",
    "influx_air_minute = 6           # liters\n",
    "breaths_minute = 20             # breaths\n",
    "liters_per_breath = influx_air_minute / breaths_minute # liters\n",
    "liters_O2_per_breath = liters_per_breath * oxygen_partial_pressure\n",
    "mols_oxygen_per_breath = mol_oxygen_per_liter_air * liters_per_breath\n",
    "\n",
    "print('Estimate #1 from total air influx per minute and number of breaths')\n",
    "print('%.1f liters of air per breath' % liters_per_breath)\n",
    "print('%.1f liters O2 per breath' % liters_O2_per_breath)\n",
    "print('%.1g mols O2 per breath' % mols_oxygen_per_breath)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Strategy 2\n",
    "Look up the adult human \"tidal volume\" which is what the size of a breath is called. The estimate above agrees just fine with the [tidal volume](https://en.wikipedia.org/wiki/Tidal_volume) article on Wikipedia which gives 0.5 L of air per breath. \n",
    "\n",
    "Checking some literature it seems like 500 mL is a very good estimate. A mathematical model by Hoffman projects 500 mL as the value for an adult human, with a range of about 300-700 mL. \n",
    "Hofmann, W. “[Mathematical Model for the Postnatal Growth of the Human Lung](https://www.ncbi.nlm.nih.gov/pubmed/7146643).” Respiration Physiology, vol. 49, no. 1, Elsevier, July 1982, "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Estimate #2 from the assumption of 500 mL air influx per breath (tidal volume)\n",
      "0.5 liters of air per breath\n",
      "0.1 liters O2 per breath\n",
      "0.004 mols O2 per breath\n"
     ]
    }
   ],
   "source": [
    "# Assuming tidal volume of 500 mL\n",
    "liters_per_breath = 0.5 # liters\n",
    "liters_O2_per_breath = liters_per_breath * oxygen_partial_pressure\n",
    "mols_oxygen_per_breath = mol_oxygen_per_liter_air * liters_per_breath\n",
    "\n",
    "print('Estimate #2 from the assumption of 500 mL air influx per breath (tidal volume)')\n",
    "print('%.1f liters of air per breath' % liters_per_breath)\n",
    "print('%.1f liters O2 per breath' % liters_O2_per_breath)\n",
    "print('%.1g mols O2 per breath' % mols_oxygen_per_breath)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on these two estimates, it seems 1 breath contains $3-4\\times10^{-3}$ moles O<sub>2</sub>. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Oxygen production of a single cyanobacterium\n",
    "\n",
    "### Strategy 1\n",
    "\n",
    "Look up literature estimates. Kihara et al. 2014 (ref below) give estimates of the O<sub>2</sub> production rate in two divergent cyanobacteria: Gloeobacter violaceus and Synechococcus elongatus PCC 7942. Gloeobacter is a very basal clade of cyanobacteria since it does not have thylakoid membranes. [Synechococcus PCC 7942](https://genome.jgi.doe.gov/portal/synel/synel.home.html) is a common model freshwater cyanobacterium. PCC 7942 was the first cyano shown to be genetically transformable and remains a mainstay of cyanobacterial research.\n",
    "\n",
    "Kihara calculate these values on the basis of two independent measurements (1) a whole culture measurement of O<sub>2</sub> production in units of $\\mu$ mol O<sub>2</sub> per mg chlorophyll per hour and (2) the mass of chlorophyll per cell. Multiplication and unit conversion gives a per-cell O<sub>2</sub> production rate in units of mols per second. \n",
    "\n",
    "| Organism | Photosynthetic Rate (mol O<sub>2</sub>/s) | O<sub>2</sub> production ($\\mu$ mol/mg Chl/hr) | Chlorphyll (molecules/cell) |\n",
    "| --- | ----------- | ----------- | ----------- |\n",
    "| Gloeobacter violaceus | $6.2\\times10^{-19}$ | 125 | $1.2\\times10^7$ |\n",
    "| S. elongatus PCC 7942 | $2.7\\times10^{-18}$ | 250 | $1.4\\times10^7$ |\n",
    "\n",
    "Kihara, Shigeharu, et al. “[Oxygen Concentration inside a Functioning Photosynthetic Cell](https://www.ncbi.nlm.nih.gov/pubmed/24806920).” Biophysical Journal, vol. 106, no. 9, May 2014, pp. 1882–89, doi:10.1016/j.bpj.2014.03.031. \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Strategy 2\n",
    "\n",
    "Perform the same estimation for another organism to make sure that we can something on the same order of magnitude. Whitehead et al. 2014 (ref below) give relevant numbers for a Synechococcus elongatus PCC 7942 and Cyanobium PCC 7001. Cyanobium is a marine cyanobacterium that is quite diverged from PCC 7942. I've repeated the same calculation for these organisms below and it gives nice agreement with the numbers in Kihara \n",
    "\n",
    "| Organism | Photosynthetic Rate (mol O<sub>2</sub>/s) | O<sub>2</sub> production ($\\mu$ mol/mg Chl/hr) | Chlorphyll (mg/cell) |\n",
    "| --- | ----------- | ----------- | ----------- |\n",
    "| Cyanobium PCC 7001 | $3.1\\times10^{-19}$ | 200 | $5.6\\times10^{-12}$ |\n",
    "| S. elongatus PCC 7942 | $6.2\\times10^{-19}$ | 200 | $11.1\\times10^{-12}$ |\n",
    "\n",
    "Whitehead, Lynne, et al. “[Comparing the in Vivo Function of α-Carboxysomes and β-Carboxysomes in Two Model Cyanobacteria](http://www.plantphysiol.org/content/165/1/398).” Plant Physiology, vol. 165, no. 1, May 2014, pp. 398–411, doi:10.1104/pp.114.237941.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Approximation from data in Whitehead 2014\n",
      "Per cell O2 evolution rate of PCC 7942 6.2e-19\n",
      "Per cell O2 evolution rate of PCC 7001 3.1e-19\n"
     ]
    }
   ],
   "source": [
    "# Chlorophyll content from low CO2 conditions in table 1 of Whitehead et al. \n",
    "chl_content_7942 = 11.1e-12 # mg/cell\n",
    "chl_content_7001 = 5.6e-12  # mg/cell\n",
    "\n",
    "# O2 production rate in umol/mg Chl/hr approximated at 600 umol/m^2/s from figure 1 of Whitehead.  \n",
    "# 600 umol/m^2/s is a relatively high light level for model cyanobacterial growth, approaching saturation. \n",
    "o2_production_7942 = 200\n",
    "o2_production_7001 = 200\n",
    "\n",
    "# Per-cell O2 production rate in mol O2/s\n",
    "secs_per_hr = 60*60\n",
    "mol_per_umol = 1e-6\n",
    "o2_evolution_percell_7942 = o2_production_7942 * chl_content_7942 * mol_per_umol / secs_per_hr\n",
    "o2_evolution_percell_7001 = o2_production_7001 * chl_content_7001 * mol_per_umol / secs_per_hr\n",
    "\n",
    "print('Approximation from data in Whitehead 2014')\n",
    "print('Per cell O2 evolution rate of PCC 7942 %.2g' % o2_evolution_percell_7942)\n",
    "print('Per cell O2 evolution rate of PCC 7001 %.2g' % o2_evolution_percell_7001)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Strategy 3\n",
    "\n",
    "The third approach calculates the O2 production rate based on simple physiological characteristic of model cyanobacteria. A typical Cyanobacterium has a 12 hour doubling time in laboratory conditions. These bacteria are similar in size to E. coli - some are smaller and others are larger, but they are all roughly 1 $\\mu$m in length. As such we can use rules of thumb about E. coli, namely that it contains about $10^{10}$ carbon atoms ([BNID 103010](https://bionumbers.hms.harvard.edu/bionumber.aspx?&id=103010&ver=6)). In order to double its mass every 12 hours, a cyanobacterium must fix $10^10$ carbon atoms in that period, or $2\\times10^5$ carbons/s. \n",
    "\n",
    "Based on the known stoichiometry of the [photosynthetic dark reactions](https://en.wikipedia.org/wiki/Light-independent_reactions), we know that 3 turns of the Calvin-Benson-Bassham cycle fixes 3 CO<sub>2</sub> at the expense of 9 ATP and 6 NADPH. The [linear Z-scheme](https://en.wikipedia.org/wiki/Photosynthesis#Z_scheme) of the photosynthetic light reactions produces 3 ATP and 2 NADPH for each O<sub>2</sub>, meaning that every CO<sub>2</sub> fixation entails the production of at least one O<sub>2</sub>.\n",
    "\n",
    "Based on this logic I calculate a per-cell O<sub>2</sub> production rate of $3.8\\times10^{-19}$ mol/cell/s below. This value is perfectly consistent with all the numbers above, implying that $10^{-19}-10^{-18}$ mol O<sub>2</sub>/cell/s is a very good estimate of the single cell O<sub>2</sub> production rate of Cyanobacteria. Note that this is most likely an upper bound estimate of the rate in nature and over evolutionary history because (1) natural light is lower than lab conditions and fluctuates, and (2) ancient cyanobacteria were likely less efficient photosynthesizers. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Physiological estimate for per-cell O2 evolution rate: 3.8e-19\n"
     ]
    }
   ],
   "source": [
    "carbons_per_cell = 1e10\n",
    "doubling_time_hr = 12\n",
    "doubling_time_s = 12*60*60\n",
    "fixations_per_s = carbons_per_cell / (doubling_time_s)\n",
    "\n",
    "# convert to molar units\n",
    "avogadros_num = 6.02e23\n",
    "o2_per_co2 = 1.0\n",
    "mols_o2_per_s = fixations_per_s * o2_per_co2 / avogadros_num\n",
    "\n",
    "print('Physiological estimate for per-cell O2 evolution rate: %.2g' % mols_o2_per_s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Time to make 1 human breath\n",
    "\n",
    "This is just a matter of multiplication. Above we estimated 3-4 millimoles O<sub>2</sub> per breath. A cyanobacteria makes $10^{-19}-10^{-18}$ mol O<sub>2</sub>/s. As a result we estimate that it would take $10^8-10^9$ years for a single cyanobacterium to make a single human breath. Clearly there must have been a lot more cyanobacteria around to appreciably influence the atmospheric O<sub>2</sub> concentration early in Earth history, especially in the face of geochemical and biological processes that consume oxygen. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Miniumum years to make 1 human breath 9.5e+07\n",
      "Maximum years to make 1 human breath 1.3e+09\n"
     ]
    }
   ],
   "source": [
    "o2_min_breath = 3e-3  # moles\n",
    "o2_max_breath = 4e-3  # moles\n",
    "o2_min_cyano  = 1e-19 # moles/s \n",
    "o2_max_cyano  = 1e-18 # moles/s\n",
    "\n",
    "secs_per_hr = 60*60\n",
    "seconds_breath_min = o2_min_breath / o2_max_cyano\n",
    "seconds_breath_max = o2_max_breath / o2_min_cyano\n",
    "hours_breath_min = seconds_breath_min / secs_per_hr\n",
    "hours_breath_max = seconds_breath_max / secs_per_hr\n",
    "\n",
    "hours_per_year = 365*24\n",
    "years_breath_min = hours_breath_min / hours_per_year\n",
    "years_breath_max = hours_breath_max / hours_per_year\n",
    "\n",
    "print('Miniumum years to make 1 human breath %.2g' % years_breath_min)\n",
    "print('Maximum years to make 1 human breath %.2g' % years_breath_max)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
